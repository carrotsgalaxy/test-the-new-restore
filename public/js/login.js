$('#submit').click(function() {
  var email = $('#email').val();
  var password = $('#password').val();
  postData('/auth_api/login_account', {"email" : email, "password" : password}, function(json) {
    if(json.success == true) {
      token = json.token;
      createCookie('authentication_token', token, 1);
      swal({
        title: 'Success!',
        text: 'Press "Okay" to be taken to the dashboard!',
        type: 'success',
        confirmButtonText: 'Okay',
        showCancelButton: false,
        allowOutsideClick: false
      }).then(function () {
        window.location = json.url;
      }).catch(swal.noop);
    } else {
      swal({
        title: 'Error',
        text: json.msg,
        type: 'error',
        confirmButtonText: 'Try Again',
        showCancelButton: false,
        allowOutsideClick: false
      }).catch(swal.noop);
    }
  });
});
