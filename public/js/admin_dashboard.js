$.datepicker.setDefaults({ dateFormat: 'yy-mm-dd' });
var types_arr = convertTypesToJson(types);
function view() {
  var numItems = $('#dashboard_table input:checked').length;
  if(numItems == 1) {
    $('#dashboard_table input:checked').each(function(i,obj) {
        var apid = $(obj).val()
        var newtab = window.open( '', '_blank' );
        newtab.location = getUrl() + '/main/view_appointment/' + apid;
    });
  } else {
    swal({
      title: 'Oops!',
      type: 'error',
      html:
        "Please select an appointment.",
      confirmButtonText: 'Ok',
      showCancelButton: false,
      allowOutsideClick: false
    }).catch(swal.noop);
  }
  /*$('#dashboard_table input:checked').each(function(i,obj) {
      var apid = $(obj).val()
  });*/
}
function edit() {
  var numItems = $('#dashboard_table input:checked').length;
  if(numItems == 1) {
    var apid = $('#dashboard_table input:checked').first().val()
    getData(getUrl() + "/api/get_appointment/" + apid, function(response) {
      if(response.response != 1) {
        var appointment = response;
        var div = "<div class='form-group'>";
        var divclose = "</div>"
        var select = div + "<label for='time' style='text-align:left;'>Time</label><select id='time' class='form-control sw2 input-lg' option="+appointment.time+">" + divclose;
        for(var i = 0; i < times.length; i++) {
          if(response.time == times[i]) {
            select = select + "<option selected value='" + times[i] + "'>"+ times[i] +"</option>"
          } else {
            select = select + "<option value='" + times[i] + "'>"+ times[i] +"</option>"
          }
          if(i+1 == times.length) {
            select = select + "</select></div>";
          }
        }
        var select_date =  div + "<label for='datepicker2' style='text-align:left;'>Date</label><input id='datepicker2' value="+appointment.date+" class='form-control input-lg' placeholder='Click to select a date...' type='text'>" + divclose;
        var additional_details = div + "<label for='additional_details' style='text-align:left;'>Description</label><textarea rows='5' class='form-control' id='additional_details'>"+appointment.additional_details+"</textarea>" + divclose;
        var select_status = div + "<label for='status' style='text-align:left;'>Status</label><select id='status' class='form-control sw2 input-lg' option="+appointment.status+">";
        for(var i = 0; i < stats.length; i++) {
          if(response.status == stats[i].id) {
            select_status = select_status + "<option selected value='" + stats[i].id + "'><font color='" + stats[i].color + "'>"+ stats[i].name +"</font></option>"
          } else {
            select_status = select_status + "<option value='" + stats[i].id + "'><font color='" + stats[i].color + "'>"+ stats[i].name +"</font></option>"
          }
          if(i+1 == stats.length) {
            select_status = select_status + "</select></div>";
          }
        }

        var select_type = div + "<label for='type' style='text-align:left;'>Type</label><select id='type' class='form-control sw2 input-lg' option="+appointment.type+">";
        for(var i = 0; i < types.length; i++) {
          if(response.type == types[i].id) {
            select_type = select_type + "<option selected value='" + types[i].id + "'>"+ types[i].name +"</option>"
          } else {
            select_type = select_type + "<option value='" + types[i].id + "'>"+ types[i].name +"</option>"
          }
          if(i+1 == types.length) {
            select_type = select_type + "</select></div>";
          }
        }
        swal({
          title: 'Editing Appointment',
          type: 'info',
          width: 600,
          confirmButtonText: 'SAVE CHANGES',
          html:
            select +
            select_date +
            select_status +
            select_type +
            additional_details,
          preConfirm: function () {
            return new Promise(function (resolve, reject) {
              if($('#datepicker2').val() != '' && $('#time').val() != '') {
                resolve([
                  $('#time').val(),
                  $('#datepicker2').val(),
                  $('#status').val(),
                  $('#type').val(),
                  $('#additional_details').val()
                ])
              } else {
                reject('Please fill in all fields');
              }
            })
          },
          onOpen: function () {
            $('#datepicker2').datepicker();
          }
        }).then(function (result) {
          var data = {
            _id: apid,
            time: result[0],
            date: result[1],
            status: result[2],
            type: result[3],
            additional_details: result[4]
          }

          postData(getUrl() + "/admin_api/edit_appointment", data, function(result) {
            if(result.response == 0) {
              swal({
                title: 'Success!',
                type: 'success',
                html:
                  'Changes have been saved.',
                confirmButtonText: 'OK',
                showCancelButton: false,
                allowOutsideClick: false
              }).then(function () {
                location.reload();
              }).catch(swal.noop);
            } else {
              swal({
                title: 'Oops!',
                type: 'error',
                html:
                  result.message,
                confirmButtonText: 'Ok',
                showCancelButton: false,
                allowOutsideClick: false
              }).catch(swal.noop);
            }
          });
        }).catch(swal.noop)
      }
    });
  } else {
    swal({
      title: 'Oops!',
      type: 'error',
      html:
        "Please select only one appointment.",
      confirmButtonText: 'Ok',
      showCancelButton: false,
      allowOutsideClick: false
    }).catch(swal.noop);
  }
  /*$('#dashboard_table input:checked').each(function(i,obj) {
      var apid = $(obj).val()
  });*/
}
function delete_app() {
  var numItems = $('#dashboard_table input:checked').length;
  if(numItems >= 1) {
    swal({
      title: 'Confirm?',
      type: 'info',
      html:
        "Are you sure you want to delete " + numItems + " appointment(s)?",
      confirmButtonText: 'Yes',
      showCancelButton: true,
      allowOutsideClick: false
    }).then(function () {
      $('#dashboard_table input:checked').each(function(i,obj) {
          var apid = $(obj).val()
          getData(getUrl() + "/admin_api/delete_appointment/" + apid, function(response) {
            if(response.response == 1) {
              swal({
                title: 'Error',
                text: response.message,
                type: 'error',
                confirmButtonText: 'Oops?',
                showCancelButton: false,
                allowOutsideClick: false
              }).catch(swal.noop);
            } else {
              if((i+1) == numItems) {
                location.reload();
              }
            }
          });
      });
    }).catch(swal.noop);
  } else {
    swal({
      title: 'Oops!',
      type: 'error',
      html:
        "Please select an appointment.",
      confirmButtonText: 'Ok',
      showCancelButton: false,
      allowOutsideClick: false
    }).catch(swal.noop);
  }
}
$(document).ready(function () {
  $('.parent_name').each(function(i, obj) {
    var id = $(obj).text();
    getData(getUrl() + '/api/get_user_data/' + id, function(callback) {
      if(callback.response != 1) {
        $(obj).text(callback.name);
      }
    });
  });

  $('.student_name').each(function(i, obj) {
    var id = $(obj).text();
    getData(getUrl() + '/api/get_student_data/' + id, function(callback) {
      if(callback.response != 1) {
        $(obj).text(callback.name);
      }
    });
  });

  $('.teacher_name').each(function(i, obj) {
    var id = $(obj).text();
    getData(getUrl() + '/api/get_user_data/' + id, function(callback) {
      if(callback.response != 1) {
        $(obj).text(callback.name);
      }
    });
  });

  $('.checkAll').on('click', function () {
    $(this).closest('table').find('tbody :checkbox')
      .prop('checked', this.checked)
      .closest('tr').toggleClass('selected', this.checked);
  });

  $('tbody :checkbox').on('click', function () {
    $(this).closest('tr').toggleClass('selected', this.checked);
    $(this).closest('table').find('.checkAll').prop('checked', ($(this).closest('table').find('tbody :checkbox:checked').length == $(this).closest('table').find('tbody :checkbox').length)); //Tira / coloca a seleção no .checkAll
  });
});

$('.avatar-student-year').each(function(i,obj) {
  uuid = children[i]._id;
  getData(getUrl() + "/api/get_year_data/"+uuid, function(result) {
    $(obj).text(result.name);
  });
});
