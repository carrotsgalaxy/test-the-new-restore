# README #
**NPM INSTALLATIONS**

```
$ git clone https://charliejamesdevelopment@bitbucket.org/charliejamesdevelopment/school-app.git
$ cd school-app
$ npm install && npm start
```


### RESTful APIs ###

- Soon-to-be updated

### How do I get set up? ###

* Summary of set up
* Database Configuration
* Dependencies

### Summary of set up ###

Install specified NPM modules, and run application. Specify database in the /routes/database/connect.js - default is mongodb://localhost/db
- Make sure to change settings to how you desire in config.ini!
```
$ git clone https://charliejamesdevelopment@bitbucket.org/charliejamesdevelopment/school-app.git
$ cd school-app
$ npm install && npm start
```

### Dependencies ###
```
#!json
{
  "dependencies": {
    "async": "^2.4.1",
    "bcrypt": "^1.0.2",
    "bcrypt-nodejs": "0.0.3",
    "body-parser": "^1.17.2",
    "connect-flash": "^0.1.1",
    "cookie-parser": "^1.4.3",
    "cookie-session": "^2.0.0-beta.2",
    "crypto": "0.0.3",
    "ejs": "^1.0.0",
    "express": "^4.0.0",
    "express-session": "^1.15.3",
    "jsesc": "^2.5.1",
    "jsonwebtoken": "^7.4.1",
    "moment": "^2.18.1",
    "mongodb": "^2.2.28",
    "mongoose": "^4.10.5",
    "morgan": "^1.8.2",
    "path": "^0.12.7",
    "serve-favicon": "^2.4.3",
    "socketio": "^1.0.0"
  }
}

```