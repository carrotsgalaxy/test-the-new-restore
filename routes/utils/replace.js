function Replace(str) {
  str.replace(/[\\$'"]/g, "\\$&");
  return str;
}

module.exports = Replace;
