var express = require('express');
var router = express.Router();
var utils = require('./utils/utils');


router.get('/',function(req,res){
  utils.isLoggedIn(req,function(call) {
    if(call == true) {
      res.redirect('/main/dashboard/false');
    } else {
      res.render('index.ejs', {
        message: req.flash('message'),
        message_type: req.flash('message_type')
      })
    }
  });
});

module.exports = router;
