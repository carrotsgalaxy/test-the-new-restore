var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var appointmentSchema = new Schema({
  parent_id: { type: String, required: true},
  teacher_id: { type: String, required: true },
  student_id: { type: String, required: true },
  time: { type: String, required: true },
  date: { type: String, required: true },
  status: { type: Number, required: true},
  additional_details: { type: String, required: false},
  type: Number,
  created_at: Date,
  updated_at: Date
});

appointmentSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});
appointmentSchema.set('collection', 'appointments');

var User = mongoose.model('Appointment', appointmentSchema);

// make this available to our users in our Node applications
module.exports = User;
