var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var config = require('../../config.js');
var appointment_utils = require('../utils/appointments.js');

router.get('/',function(req,res){
  utils.isLoggedIn(req,function(call) {
    if(call == false) {
      res.redirect('/');
    } else {
      utils.getChildrenByUUID(req.session.uuid, function(data) {
        utils.getUserData(req.session.uuid, function(obj) {
          console.log(obj);
          utils.getGroupByUUID(req.session.uuid, function(g) {
            res.render('auth/account', {
              getStatus: appointment_utils.getStatus,
              getAppointmentStatuses: appointment_utils.getAppointmentStatuses,
              times: config.appointment.times,
              types: config.appointment.types,
              user: obj,
              group: g,
              data: data,
              active: "My Account",
              admin: g.admin
            });
          });
        });
      });
    }
  });
});

module.exports = router;
