var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');
var moment = require("moment");
var config = require('../../config.js');
var appointment_utils = require('../utils/appointments.js');
router.get('/:id',function(req,res){
  if(req.session.uuid) {
    res.status(200)
    utils.getReportData(req.params.id, function(report) {
      if(report == 1) {
        res.redirect("/");
      } else {
        utils.getChildrenByUUID(req.session.uuid, function(data) {
          utils.getUserData(req.session.uuid, function(obj) {
            utils.getGroupByUUID(req.session.uuid, function(g) {
              res.render('main/view_report', {
                getStatus: appointment_utils.getStatus,
                getAppointmentStatuses: appointment_utils.getAppointmentStatuses,
                times: config.appointment.times,
                types: config.appointment.types,
                user: obj,
                group: g.name,
                admin: g.admin,
                data: data,
                report: report,
                active: "Reports",
                moment: moment
              });
            });
          });
        });
      }
    });
  } else {
    res.redirect("/");
  }
});

module.exports = router;
