var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');
var config = require('../../config.js');
var appointment_utils = require('../utils/appointments.js');
router.get('/:id',function(req,res){
  if(req.session.uuid) {
    res.status(200)
    utils.getAppointmentData(req.params.id, function(appointment) {
      if(appointment == 1) {
        res.redirect("/");
      } else {
        utils.getChildrenByUUID(req.session.uuid, function(data) {
          utils.getUserData(req.session.uuid, function(obj) {
            utils.getGroupByUUID(req.session.uuid, function(g) {
              if(g.admin == true || appointment.parent_id == req.session.uuid
              || appointment.teacher_id == req.session.uuid) {
                res.render('main/view_appointment', {
                  getStatus: appointment_utils.getStatus,
                  getAppointmentStatuses: appointment_utils.getAppointmentStatuses,
                  times: config.appointment.times,
                  types: config.appointment.types,
                  admin: g.admin,
                  user: obj,
                  group: g.name,
                  data: data,
                  appointment: appointment,
                  active: "Appointments"
                });
              } else {
                res.redirect("/");
              }
            });
          });
        });
      }
    });
  } else {
    res.redirect("/");
  }
});

module.exports = router;
