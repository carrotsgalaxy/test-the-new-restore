var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');
router.get('/get_group_by_id/:id',function(req,res){
    utils.getGroupByGroupId(req.params.id, function(call) {
      if(call == 1) {
        res.send({"response" : 1, "message" : "Invalid id!"});
      } else {
        res.send(call);
      }
    });
});

module.exports = router;
